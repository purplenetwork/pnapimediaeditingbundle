<?php

namespace PN\APIMediaEditingBundle\Audio;

use PN\APIMediaEditingBundle\Audio\Exception\AudioException;
use PN\APIMediaEditingBundle\Services\MediaUtilsService;
use Psr\Log\LoggerInterface;

/**
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
class EditAudio implements EditAudioInterface
{
    protected $ffmpegPath;
    protected $ffmpegThreads;
    protected $ffprobePath;

    /** @var LoggerInterface */
    protected $logger;

    /** @var MediaUtilsService */
    protected $mediaUtils;

    public function __construct($ffmpegPath, $ffprobePath, $ffmpegThreads, LoggerInterface $logger, MediaUtilsService $mediaUtils)
    {
        $this->ffmpegPath = $ffmpegPath;
        $this->ffprobePath = $ffprobePath;
        $this->ffmpegThreads = $ffmpegThreads;
        $this->logger = $logger;
        $this->mediaUtils = $mediaUtils;
    }

    /**
     * @inheritDoc
     */
    public function convertToMP3($filePath)
    {
        $this->mediaUtils->isFile($filePath);

        //Specifico il file sorgente
        $command = $this->ffmpegPath . " -i " . $filePath;
        //Indico quale libreria utilizzare nell'output
        $command .= " -ar 44100 -ac 2 -ab 128k -f mp3";
        //Specifico il numero dei threads
        $command .= " -threads " . $this->ffmpegThreads;

        return $this->finalizeCommand($filePath, $command, 'mp3', false);
    }

    protected function finalizeCommand($filePath, $command, $ext = null, $isTemp = true)
    {
        $finalWrite = $this->mediaUtils->getFinalFileName($filePath, $ext, $isTemp);
        $command .= " -y " . $finalWrite;
        $this->executeCommand($command);
        return $finalWrite;
    }

    protected function executeCommand($cmd, $raiseError = true)
    {
        $output = null;
        $retval = -1;
        $this->logger->notice('[FFMPEG COMMAND EXECUTE]: ' . $cmd);
        exec($cmd . " 2>&1", $output, $retval);
        if ($retval != 0 && $raiseError) {
            $this->logger->error('[FFMPEG COMMAND ERROR]');
            $this->logger->error(print_r($output, TRUE));
            throw new AudioException("Ops, ffmpeg command failure!");
        }
        return array($output, $retval);
    }
}