<?php

namespace PN\APIMediaEditingBundle\Audio\Exception;

/**
 * Description of AudioException
 *
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
class AudioException extends \Exception {
    
}
