<?php

namespace PN\APIMediaEditingBundle\Audio;

/**
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
interface EditAudioInterface
{

    /**
     * Convert a given file to MP3
     *
     * @param $filePath
     * @return mixed
     */
    public function convertToMP3($filePath);
}
