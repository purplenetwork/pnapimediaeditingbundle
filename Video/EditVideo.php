<?php

namespace PN\APIMediaEditingBundle\Video;

use PN\APIMediaEditingBundle\Services\MediaUtilsService;
use PN\APIMediaEditingBundle\Video\Exception\FfmpegException;
use Psr\Log\LoggerInterface;

/**
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
class EditVideo implements EditVideoInterface
{

    protected $ffmpegPath;
    protected $ffmpegThreads;
    protected $ffprobePath;

    /** @var LoggerInterface */
    protected $logger;

    /** @var MediaUtilsService */
    protected $mediaUtils;

    public function __construct($ffmpegPath, $ffprobePath, $ffmpegThreads, LoggerInterface $logger, MediaUtilsService $mediaUtils)
    {
        $this->ffmpegPath = $ffmpegPath;
        $this->ffprobePath = $ffprobePath;
        $this->ffmpegThreads = $ffmpegThreads;
        $this->logger = $logger;
        $this->mediaUtils = $mediaUtils;
    }

    protected function finalizeCommand($filePath, $command, $ext = null, $isTemp = true, $usePreviousFileName = false)
    {
        $finalWrite = $this->mediaUtils->getFinalFileName($filePath, $ext, $isTemp, $usePreviousFileName);
        $command .= " -y " . $finalWrite;
        $this->executeCommand($command);
        return $finalWrite;
    }

    protected function verifyIfFileExists($filePath)
    {
        $this->mediaUtils->isFile($filePath);
    }

    public function executeCommand($cmd, $raiseError = true)
    {
        $output = null;
        $retval = -1;
        $this->logger->notice('[FFMPEG COMMAND EXECUTE]: ' . $cmd);
        exec($cmd . " 2>&1", $output, $retval);
        if ($retval != 0 && $raiseError) {
            $this->logger->error('[FFMPEG COMMAND ERROR]');
            $this->logger->error(print_r($output, TRUE));
            throw new FfmpegException("Ops, ffmpeg command failure!");
        }
        return array($output, $retval);
    }

    public function hasStreamAudioAndVideo($filePath)
    {
        $this->verifyIfFileExists($filePath);

        $command = $this->ffprobePath;
        $command .= " -v quiet -print_format json -show_format -show_streams ";
        $command .= $filePath;
        list($output, $retval) = $this->executeCommand($command, false);

        $video = $audio = false;
        foreach ($output as $row) {
            if (strpos($row, '"codec_type": "video"')) {
                $video = true;
            }

            if (strpos($row, '"codec_type": "audio"')) {
                $audio = true;
            }
        }

        return array($video, $audio);
    }

    public function trimMp4($filePath, $start, $length)
    {
        $this->verifyIfFileExists($filePath);
        /*
         * Dopo aver notato che se eseguo un trim, perdo molta qualità del video e dell'audio
         * devo forzare il reencoding del video
         */
        $command = $this->ffmpegPath . " -i " . $filePath . " -ss " . $start . " -t " . $length;
        $command .= " -c:v libx264";
        $command .= " -threads " . $this->ffmpegThreads;
        $command .= " -profile:v main -level 3.1 -preset medium -r 25 -b:v 1200k -minrate 1200k -maxrate 1200k -bufsize 1200k";
        //Impostazioni audio output 
        $command .= " -c:a libfdk_aac -ar 48000 -ac 2 -ab 128k";

        return $this->finalizeCommand($filePath, $command);
    }

    public function trimWebm($filePath, $start, $length)
    {
        $this->verifyIfFileExists($filePath);
        /*
         * Dopo aver notato che se eseguo un trim, perdo molta qualità del video e dell'audio
         * devo forzare il reencoding del video
         */
        $command = $this->ffmpegPath . " -i " . $filePath . " -ss " . $start . " -t " . $length;
        $command .= " -c:v libvpx";
        $command .= " -threads " . $this->ffmpegThreads;
        $command .= " -crf 10 -r 25"; //-r frame per secondo https://trac.ffmpeg.org/wiki/Encode/VP8
        $command .= " -c:a libvorbis -ar 48000 -ac 2 -ab 128k";

        return $this->finalizeCommand($filePath, $command);
    }

    public function getPreviewImage($filePath, $second = null)
    {
        $this->verifyIfFileExists($filePath);

        $second = empty($second) ? floor($this->getVideoLength($filePath) / 2) : $second;
        $command = $this->ffmpegPath . " -i " . $filePath . " -ss " . $second . " -f mjpeg -vframes 1";

        return $this->finalizeCommand($filePath, $command, 'jpg', false, true);
    }

    public function getVideoLength($filePath)
    {
        $this->verifyIfFileExists($filePath);

        $command = $this->ffmpegPath . " -i " . $filePath;
        list($output, $retval) = $this->executeCommand($command, false);
        $total = 0;
        foreach ($output as $row) {
            if (preg_match('/Duration: ((\d+):(\d+):(\d+))/s', $row, $time)) {
                $total = ($time[2] * 3600) + ($time[3] * 60) + $time[4];
            }
        }

        if ($total == 0) {
            throw new FfmpegException('Video has no length');
        }

        return $total;
    }

    public function convertToMpeg($filePath, $width, $height)
    {
        $this->verifyIfFileExists($filePath);

        //Specifico il file sorgente
        $command = $this->ffmpegPath . " -i " . $filePath;
        $command .= " -q:v 1 -maxrate 15000k -bufsize 15000k " . " -s " . $width . "x" . $height;

        return $this->finalizeCommand($filePath, $command);
    }

    public function convertToMp4($filePath, $width, $height)
    {
        $this->verifyIfFileExists($filePath);

        //Specifico il file sorgente
        $command = $this->ffmpegPath . " -i " . $filePath;
        //Indico quale libreria utilizzare nell'output
        $command .= " -c:v libx264";
        //Specifico il numero dei threads
        $command .= " -threads " . $this->ffmpegThreads;
        //Impostazioni video output
        $command .= " -profile:v main -preset medium -r 25 -b:v 1200k -minrate 1200k -maxrate 1200k -bufsize 1200k -pix_fmt yuv420p -s " . $width . "x" . $height;
        //Impostazioni audio output
        $command .= " -c:a libfdk_aac -ar 48000 -ac 2 -ab 128k";

        return $this->finalizeCommand($filePath, $command, 'mp4', false);
    }

    public function convertToWebm($filePath, $width, $height)
    {
        $this->verifyIfFileExists($filePath);

        //Specifico il file sorgente
        $command = $this->ffmpegPath . " -i " . $filePath;
        //Indico quale libreria utilizzare nell'output
        $command .= " -c:v libvpx";
        //Specifico il numero dei threads
        $command .= " -threads " . $this->ffmpegThreads;
        //Impostazioni video output
        $command .= " -crf 10 -r 25 -s " . $width . "x" . $height; //-r frame per secondo https://trac.ffmpeg.org/wiki/Encode/VP8
        //Impostazioni audio output
        $command .= " -c:a libvorbis -ar 48000 -ac 2 -ab 128k";

        return $this->finalizeCommand($filePath, $command);
    }

    public function fixOrientation($filePath)
    {
        $this->verifyIfFileExists($filePath);

        // get rotation of the video
        ob_start();
        passthru($this->ffmpegPath . " -i " . $filePath . " 2>&1");
        $duration_output = ob_get_contents();
        ob_end_clean();

        if (preg_match('/ffmpeg: not found(.*?)\n/', $duration_output, $matches)) {
            throw new FfmpegException('FFmpeg not found');
        }

        /*
         * 0 = 90CounterCLockwise and Vertical Flip (default)
         * 1 = 90Clockwise
         * 2 = 90CounterClockwise
         * 3 = 90Clockwise and Vertical Flip
         */
        if (preg_match('/rotate *: (.*?)\n/', $duration_output, $matches)) {
            $rotation = $matches[1];
            $command = null;
            if ($rotation == "90") {
                $command = $this->ffmpegPath . ' -noautorotate -i ' . $filePath . ' -metadata:s:v:0 rotate=0 -vf "transpose=1" -threads ' . $this->ffmpegThreads;
            } else if ($rotation == "180") {
                $command = $this->ffmpegPath . ' -noautorotate -i ' . $filePath . ' -metadata:s:v:0 rotate=0 -vf "transpose=2,transpose=2" -threads ' . $this->ffmpegThreads;
            } else if ($rotation == "270") {
                $command = $this->ffmpegPath . ' -noautorotate -i ' . $filePath . ' -metadata:s:v:0 rotate=0 -vf "transpose=2" -threads ' . $this->ffmpegThreads;
            }

            if (null !== $command) {
                $fileTo = $this->finalizeCommand($filePath, $command);
                $this->executeCommand("mv " . $fileTo . " " . $filePath, true);
            }
        }

        return $filePath;
    }

    public function getVideoSize($filePath)
    {
        $this->verifyIfFileExists($filePath);

        $command = $this->ffprobePath;
        $command .= " -v quiet -print_format json -show_format -show_streams ";
        $command .= $filePath;
        ob_start();
        passthru($command);
        $output = ob_get_contents();
        ob_end_clean();

        $videoInfo = json_decode($output);
        if (!$videoInfo) {
            throw new FfmpegException("Impossibile to read video size.");
        }

        $width = $height = 0;
        foreach ($videoInfo->streams as $stream) {
            if ($stream->codec_type == 'video') {
                $width = intval($stream->width);
                $height = intval($stream->height);
            }
        }

        return array($width, $height);
    }

}
