<?php

namespace PN\APIMediaEditingBundle\Video;

/**
 * Description of EditVideoInterface
 *
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
interface EditVideoInterface
{

    /**
     * Execute a ffmpeg command
     *
     * @param string $command Ffmpeg command
     * @param boolean $raiseError Should launch Exceptions?
     *
     * @return array First element is output, second the return value
     * @throws \RuntimeException
     */
    public function executeCommand($command, $raiseError = true);

    /**
     * Convert video file to mp4
     *
     * @param string $filePath Source file
     * @param string $width Width size file mp4
     * @param string $height Height size file mp4
     * @param string $fileTo Destination path file
     *
     * @return string File mp4 path
     * @throws \RuntimeException
     */
    public function convertToMp4($filePath, $width, $height);

    /**
     * Convert video file to mp4
     *
     * @param string $filePath Source file
     * @param string $width Width size file mp4
     * @param string $height Height size file mp4
     *
     * @return string File webm path
     * @throws \RuntimeException
     */
    public function convertToWebm($filePath, $width, $height);

    /**
     * Convert video file to mp4
     *
     * @param string $filePath Source file
     * @param int $second Source frame
     *
     * @return string File preview path
     * @throws \RuntimeException
     */
    public function getPreviewImage($filePath, $second = null);

    /**
     * Get the length of a video
     *
     * @param string $filePath Source file
     *
     * @return int Video length
     * @throws \RuntimeException
     */
    public function getVideoLength($filePath);

    /**
     * Convert video to mpeg
     *
     * @param string $filePath Source file
     * @param string $width Width size file mp4
     * @param string $height Height size file mp4
     *
     * @return string File preview path
     * @throws \RuntimeException
     */
    public function convertToMpeg($filePath, $width, $height);

    /**
     * Trim a mp4 video
     *
     * @param string $filePath Source file
     * @param string $start Trim start
     * @param string $length Trim length
     *
     * @return string File trimmed path
     * @throws \RuntimeException
     */
    public function trimMp4($filePath, $start, $length);

    /**
     * Trim a webm video
     *
     * @param string $filePath Source file
     * @param string $start Trim start
     * @param string $length Trim length
     *
     * @return string File trimmed path
     * @throws \RuntimeException
     */
    public function trimWebm($filePath, $start, $length);

    /**
     * Return if a file has both audio and video stream
     *
     * @param string $filePath Source file
     *
     * @return boolean
     * @throws \RuntimeException
     */
    public function hasStreamAudioAndVideo($filePath);

    /**
     * Fix video orientation
     *
     * @param string $filePath Source file
     *
     * @return string File mp4 path
     * @throws \RuntimeException
     */
    public function fixOrientation($filePath);

    /**
     * Get Video Resolution
     *
     * @param string $filePath Source file
     *
     * @return array width, height
     * @throws \RuntimeException
     */
    public function getVideoSize($filePath);
}
