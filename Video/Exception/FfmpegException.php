<?php

namespace PN\APIMediaEditingBundle\Video\Exception;

/**
 * Description of FfmpegException
 *
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
class FfmpegException extends \Exception {
    
}