<?php

namespace PN\APIMediaEditingBundle\Services;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
interface MediaUtilsServiceInterface
{
    public function getNameWithoutExtension($filename);

    public function getFilenameFromPath($filename);

    public function getFileExtension($filename);

    public function getDirectoryFromFile($filename);

    /**
     * Check if the file exists
     *
     * @param $filePath
     *
     * @throws FileNotFoundException
     */
    public function isFile($filePath);

    public function getFinalFileName($filename, $ext = null, $isTemp = true);

    public function getAbsolutePathUploadsFolder();

    public function getAbsolutePathWorkingFolderFromFileAndBucket($url, $bucket);

    public function getRelativePathWorkingFolderFromFileAndBucket($url, $bucket);

    public function setupFolder($workingFolder);

    public function addSlashIfNotPresentAsFirstCharacter($path);

    /**
     * Calculate ratio from width and height provided
     *
     * @param integer $width Width of the image
     * @param integer $height Height of the image
     *
     * @return float Aspect ratio
     */
    public function getMediaRatio($width, $height);

    /**
     * Calculate new height for image that should be scaled
     *
     * @param integer $targetWidth Width of the new image
     * @param float $ratio Image's ratio
     *
     * @return integer The height to use for the new image
     */
    public function getNewHeightForScale($targetWidth, $ratio);

    /**
     * Calculate new width for image that should be scaled
     *
     * @param integer $targetHeight Height of the new image
     * @param float $ratio Image's ratio
     *
     * @return integer The width to use for the new image
     */
    public function getNewWidthForScale($targetHeight, $ratio);
}
