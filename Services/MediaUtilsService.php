<?php

namespace PN\APIMediaEditingBundle\Services;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
class MediaUtilsService implements MediaUtilsServiceInterface
{
    /** @var KernelInterface */
    protected $kernelInterface;

    public function __construct(KernelInterface $kernelInterface)
    {
        $this->kernelInterface = $kernelInterface;
    }

    public function getNameWithoutExtension($filename)
    {
        return pathinfo($filename, PATHINFO_FILENAME);
    }

    public function getFilenameFromPath($filename)
    {
        return pathinfo($filename, PATHINFO_BASENAME);
    }

    public function getFileExtension($filename)
    {
        return pathinfo($filename, PATHINFO_EXTENSION);
    }

    public function getDirectoryFromFile($filename)
    {
        return dirname($filename);
    }

    public function isFile($filePath)
    {
        if (!file_exists($filePath)) {
            throw new FileNotFoundException('File not found: ' . $filePath);
        }
    }

    protected function getFileFullPathWithoutExtension($filename)
    {
        return $this->getDirectoryFromFile($filename) .
               $this->addSlashIfNotPresentAsFirstCharacter($this->getNameWithoutExtension($filename));
    }
    
    public function getFinalFileName($filename, $ext = null, $isTemp = true, $usePreviousFileName = false)
    {
        $finalFilename = $this->getFileFullPathWithoutExtension($filename);
        if ($usePreviousFileName) {
            $finalFilename .= '.';
        } else {
            $finalFilename .= $isTemp ? '_tmp.' : '_final.';
        }
        $finalFilename .= empty($ext) ? $this->getFileExtension($filename) : $ext;
        return $finalFilename;
    }

    public function getAbsolutePathUploadsFolder()
    {
        $webFolder = realpath($this->kernelInterface->getRootDir() . '/../web');
        if (!$webFolder) {
            throw new AccessDeniedHttpException('Impossible to find web folder');
        }
        return $webFolder . '/uploads';
    }

    public function getAbsolutePathWorkingFolderFromFileAndBucket($url, $bucket)
    {
        $fullPathUploadsFolder = $this->getAbsolutePathUploadsFolder();
        $relativePathWorkingFolder = $this->getRelativePathWorkingFolderFromFileAndBucket($url, $bucket);
        return $fullPathUploadsFolder .
            $this->addSlashIfNotPresentAsFirstCharacter($bucket) .
            $this->addSlashIfNotPresentAsFirstCharacter($relativePathWorkingFolder);
    }

    public function getRelativePathWorkingFolderFromFileAndBucket($url, $bucket)
    {
        //Get only relative folder
        $url = strtolower($url);
        return substr($url, strpos($url, $bucket) + strlen($bucket) + 1);
    }

    public function setupFolder($fileFullPath)
    {
        $workingFolder = $this->getDirectoryFromFile($fileFullPath);
        $isFolderAdded = true;
        if (!is_dir($workingFolder)) {
            $isFolderAdded = @mkdir($workingFolder, 0775, true);
        }

        if (!$isFolderAdded) {
            throw new AccessDeniedHttpException('Impossible to setup folder: ' . $workingFolder);
        }
    }

    public function addSlashIfNotPresentAsFirstCharacter($path)
    {
        if (substr($path, 0, 1) !== '/')
        {
            return '/' . $path;
        }
        return $path;
    }

    public function getMediaRatio($width, $height)
    {
        return $width / $height;
    }

    public function getNewHeightForScale($targetWidth, $ratio)
    {
        $newHeight = round($targetWidth / $ratio);
        $newHeight = $newHeight % 2 === 0 ? $newHeight : $newHeight - 1;
        return intval($newHeight);
    }

    public function getNewWidthForScale($targetHeight, $ratio)
    {
        $newWidth = round($targetHeight * $ratio);
        $newWidth = $newWidth % 2 === 0 ? $newWidth : $newWidth - 1;
        return intval($newWidth);
    }
}
