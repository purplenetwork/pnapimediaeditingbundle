<?php

namespace PN\APIMediaEditingBundle\Image;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

/**
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
interface EditImageInterface
{

    public function forceResize($path, $width, $height, $dest, $type = 'guess', $quality = 100);

    public function scaleResize($path, $width, $height, $dest, $type = 'guess', $quality = 100);

    public function smooth($path, $p, $type = 'guess', $quality = 100);

    /**
     * Return size of the image provided
     *
     * @param string $path Image file path
     *
     * @return array First element is width, second element is height
     */
    public function getImageSize($path);

    public function crop($path, $x, $y, $width, $height, $dest, $type = 'guess', $quality = 100);

    /**
     * Check if an image is rotated through EXIF properties and remove rotation if is present
     *
     * @param $path
     * @param $dest
     * @param string $type
     * @param int $quality
     * @return mixed
     *
     * @throws FileNotFoundException
     */
    public function fixOrientation($path, $dest, $type = 'guess', $quality = 100);

    public function zoomCrop($path, $width, $height, $dest, $type = 'guess', $quality = 100);

    /**
     * Return a color resource
     *
     * @param string $path Image file path
     * @param string $red Red RGA value
     * @param string $green Green RGA value
     * @param string $blue Blue RGA value
     *
     * @return mixed Color Resource
     */
    public function generateColorResource($path, $red, $green, $blue);

    /**
     * Generate an image resorce
     *
     * @param $type
     * @param $path
     * @return mixed
     */
    public function generateImageResource($type, $path);

    /**
     * Save an image resorce
     * 
     * @param $type
     * @param $imageResource
     * @param $imageFilename
     * @param int $quality
     * @return mixed
     */
    public function saveImageResource($type, $imageResource, $imageFilename, $quality = 100);

    public function generateImageWithText($image, $size, $angle, $x, $y, $color, $fontPath, $text);

    public function mergeImages($pathFirst, $x, $y, $width, $height, $pathSecond, $dest, $type = 'guess', $quality = 100);

    public function guessType($filename);

}
