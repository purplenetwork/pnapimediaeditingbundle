<?php

namespace PN\APIMediaEditingBundle\Image\Exception;

/**
 * Description of ImageException
 *
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
class ImageException extends \Exception {
    
}
