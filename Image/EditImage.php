<?php

namespace PN\APIMediaEditingBundle\Image;

use PN\APIMediaEditingBundle\Services\MediaUtilsService;
use Psr\Log\LoggerInterface;
use Gregwar\Image\Image;

/**
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 */
class EditImage implements EditImageInterface
{
    /** @var LoggerInterface */
    protected $logger;

    /** @var MediaUtilsService */
    protected $mediaUtils;

    public function __construct(LoggerInterface $logger, MediaUtilsService $mediaUtils)
    {
        $this->logger = $logger;
        $this->mediaUtils = $mediaUtils;
    }

    public function forceResize($path, $width, $height, $dest, $type = 'guess', $quality = 100)
    {
        $this->mediaUtils->isFile($path);
        Image::open($path)->forceResize($width, $height, 'transparent')->save($dest, $type, $quality);
    }

    public function scaleResize($path, $width, $height, $dest, $type = 'guess', $quality = 100)
    {
        $this->mediaUtils->isFile($path);
        Image::open($path)->scaleResize($width, $height, 'transparent')->save($dest, $type, $quality);
    }

    public function crop($path, $x, $y, $width, $height, $dest, $type = 'guess', $quality = 100)
    {
        $this->mediaUtils->isFile($path);
        Image::open($path)->crop($x, $y, $width, $height)->save($dest, $type, $quality);
    }

    public function fixOrientation($path, $dest, $type = 'guess', $quality = 100)
    {
        $this->mediaUtils->isFile($path);
        if (!extension_loaded('exif')) {
            throw new \RuntimeException('You need to EXIF PHP Extension to use this function');
        }

        $exif = @exif_read_data($path);

        if(!array_key_exists('Orientation', $exif)) {
            return true;
        }

        /** @var Image $image */
        $image = Image::open($path);

        switch($exif['Orientation']) {
            case 1:
                break;

            case 2:
                $image = $image->flip(false, true);
                break;

            case 3: // 180 rotate left
                $image = $image->rotate(180);
                break;

            case 4: // vertical flip
                $image = $image->flip(true, false);
                break;

            case 5: // vertical flip + 90 rotate right
                $image = $image->flip(true, false);
                $image = $image->rotate(-90);
                break;

            case 6: // 90 rotate right
                $image = $image->rotate(-90);
                break;

            case 7: // horizontal flip + 90 rotate right
                $image = $image->flip(false, true);
                $image = $image->rotate(-90);
                break;

            case 8: // 90 rotate left
                $image = $image->rotate(90);
                break;
        }
        return $image->save($dest, $type, $quality);
    }

    public function zoomCrop($path, $width, $height, $dest, $type = 'guess', $quality = 100)
    {
        $this->mediaUtils->isFile($path);
        Image::open($path)->zoomCrop($width, $height)->save($dest, $type, $quality);
    }

    public function smooth($path, $p, $type = 'guess', $quality = 100)
    {
        $this->mediaUtils->isFile($path);
        Image::open($path)->smooth($p)->save($path, $type, $quality);
    }

    public function getImageSize($path)
    {
        return getimagesize($path);
    }

    public function generateImageWithText($image, $size, $angle, $x, $y, $color, $fontPath, $text)
    {
        return imagefttext($image, $size, $angle, $x, $y, $color, $fontPath, $text);
    }

    public function generateColorResource($path, $red, $green, $blue)
    {
        return imagecolorallocate($path, $red, $green, $blue);
    }

    public function generateImageResource($type, $path)
    {
        switch ($type) {
            case 'png':
                return imagecreatefrompng($path);
            case 'gif':
                return imagecreatefromgif($path);
            default:
                return imagecreatefromjpeg($path);
        }
    }

    public function mergeImages($pathFirst, $x, $y, $width, $height, $pathSecond, $dest, $type = 'guess', $quality = 100)
    {
        $this->mediaUtils->isFile($pathFirst);
        $this->mediaUtils->isFile($pathSecond);
        Image::open($pathFirst)->merge(Image::open($pathSecond), $x, $y, $width, $height)->save($dest, $type, $quality);
    }

    public function saveImageResource($type, $imageResource, $imageFilename, $quality = 100)
    {
        switch ($type) {
            case 'png':
                return imagepng($imageResource, $imageFilename, $quality);
            case 'gif':
                return imagegif($imageResource, $imageFilename);
            default:
                return imagejpeg($imageResource, $imageFilename, $quality);
        }
    }

    public function guessType($filename)
    {
        if (function_exists('exif_imagetype')) {
            $type = @exif_imagetype($filename);

            if (false !== $type) {
                if ($type == IMAGETYPE_JPEG) {
                    return 'jpeg';
                }

                if ($type == IMAGETYPE_GIF) {
                    return 'gif';
                }

                if ($type == IMAGETYPE_PNG) {
                    return 'png';
                }
            }
        }

        $parts = explode('.', $filename);
        $ext = strtolower($parts[count($parts) - 1]);

        if (isset(Image::$types[$ext])) {
            return Image::$types[$ext];
        }

        return false;
    }

}